from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',

    url(r'^menstruation/', include('menstruation.urls')),
    url(r'^/', include('menstruation.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
