from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect,  HttpResponse
from django.core.urlresolvers import reverse

from django.views.generic import UpdateView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse_lazy
from menstruation.models import Calculator, Contact

from menstruation.forms import ContactForm
from menstruation.forms import CalculatorForm
from menstruation.forms import ConseilForm
from .models import Article

# Create your views here.




def contact(request):
    # Handle file upload
    context_dict = {'boldmessage': "insert in"}
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(" Form valide! insertion reussi appuyer sur Backspace ")
            # return render(request, 'rango/index.html', context_dict)
            return HttpResponseRedirect(reverse('menstruation.views.contact'))
        else:
             return HttpResponse(" Form invalide!")


        formc = CalculatorForm(request.POST)
        if formc.is_valid():
            formc.save()
            return HttpResponseRedirect(reverse('menstruation.views.contact'))
        else:
             return HttpResponse("Form invalide!")

    else:
        form = ContactForm()    # A empty, unbound form
        formc = CalculatorForm  # A empty, unbound form
    # year = 5
    # form_conseil = ConseilForm()
    # form_conseil = Article.objects.filter(pub_date__year=year)
    # context = {}

    return render_to_response(
        'menstruation/index.html', {'form': form, 'form1': formc}, context_instance=RequestContext(request)
        # 'menstruation/index.html', {'form': form, 'form1': formc, 'year': year, 'article_list': form_conseil}, context_instance=RequestContext(request)
    )


# class contact(UpdateView):
#
#     template_name = 'menstruation/index.html'
#     form_class =CalculatorForm
#     second_form_class =  ContactForm
#     success_url = reverse_lazy('success')
#
#     def get_context_data(self, **kwargs):
#         context = super(contact, self).get_context_data(**kwargs)
#         if 'CalculatorForm' not in context:
#             context['CalculatorForm'] = self.form_class(initial={'some_field': context['Calculator'].some_field})
#         if 'ContactForm' not in context:
#             context['ContactForm'] = self.second_form_class(initial={'another_field': context['Contact'].another_field})
#         return context
#
#     def get_object(self):
#         return get_object_or_404(Calculator, Contact, pk=self.request.session['someval'])
#
#     def form_invalid(self, **kwargs):
#         return self.render_to_response(self.get_context_data(**kwargs))
#
#     def post(self, request, *args, **kwargs):
#
#         # get the user instance
#         self.object = self.get_object()
#
#         # determine which form is being submitted
#         # uses the name of the form's submit button
#         if 'CalculatorForm' in request.POST:
#
#             # get the primary form
#             form_class = self.get_form_class()
#             form_name = 'CalculatorForm'
#
#         else:
#
#             # get the secondary form
#             form_class = self.second_form_class
#             form_name = 'ContactForm'
#
#         # get the form
#         form = self.get_form(form_class)
#
#         # validate
#         if form.is_valid():
#             return self.form_valid(form)
#         else:
#             return self.form_invalid(**{form_name: form})
