from django.db import models
from django.shortcuts import render
from django.http import HttpResponseRedirect
import datetime

# Create your models here.

class Contact(models.Model):
        name = models.CharField(max_length=50)
        email = models.EmailField(max_length=70, unique=True)
        subject = models.CharField(max_length=100)
        message = models.TextField(max_length=1000)


class Reporter(models.Model):
    full_name = models.CharField(max_length=70)

    def __str__(self):         # __unicode__ on Python 2
        return self.full_name

class Article(models.Model):
    pub_date = models.DateField()
    headline = models.CharField(max_length=200)
    content = models.TextField()
    reporter = models.ForeignKey(Reporter)

    def __str__(self):              # __unicode__ on Python 2
        return self.headline


class Conseil(models.Model):
        conseil1 = models.TextField(max_length=1000)
        conseil2 = models.TextField(max_length=1000)
        conseil3 = models.TextField(max_length=1000)
        conseil4 = models.TextField(max_length=1000)
        conseil5 = models.TextField(max_length=1000)
        conseil6 = models.TextField(max_length=1000)


class SubscribeForm(models.Model):
        name = models.CharField(max_length=50)
        email = models.EmailField(max_length=70, unique=True)


YEAR_CHOICES = []
for r in range(2005, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r, r))

DAY_CHOICES = []
for r in range(1, 32):
    DAY_CHOICES.append((r, r))

MONTH_CHOICES = []
for r in range(1, 13):
    MONTH_CHOICES.append((r, r))

DURATION_CHOICES = []
for r in range(1, 11):
    DURATION_CHOICES.append((r, r))

DURATION1_CHOICES = []
for r in range(11, 36):
    DURATION1_CHOICES.append((r, r))

class Calculator(models.Model):
    year = models.IntegerField('year', max_length=4, choices=YEAR_CHOICES, default=datetime.datetime.now().year)
    day = models.IntegerField('day', max_length=2, choices=DAY_CHOICES, default=datetime.datetime.now().day)
    month = models.CharField('month', max_length=20, choices=MONTH_CHOICES, default=datetime.datetime.now().month)
    dureRegles = models.IntegerField(max_length=2, choices=DURATION_CHOICES, default=4)
    dureCyles = models.IntegerField(max_length=2, choices=DURATION1_CHOICES, default=16)

    # annee = models.Y(max_length=1000)
    # contenu = models.TextField(null=True)
    # date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Date de parution")
