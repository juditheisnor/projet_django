# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menstruation', '0005_auto_20150301_1000'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculatorform',
            name='month',
            field=models.CharField(default=3, verbose_name='month', max_length=20, choices=[(1, 1), (2, 2), (3, 3)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='calculatorform',
            name='day',
            field=models.IntegerField(default=1, verbose_name='day', max_length=2, choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31)]),
            preserve_default=True,
        ),
    ]
