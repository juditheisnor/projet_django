# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menstruation', '0004_auto_20150301_0953'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculatorform',
            name='day',
            field=models.IntegerField(max_length=2, choices=[(1, 1)], verbose_name='day', default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='calculatorform',
            name='year',
            field=models.IntegerField(max_length=4, choices=[(2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015)], verbose_name='year', default=2015),
            preserve_default=True,
        ),
    ]
