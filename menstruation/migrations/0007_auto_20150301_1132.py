# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menstruation', '0006_auto_20150301_1127'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculatorform',
            name='duration',
            field=models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], default=4, max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='calculatorform',
            name='month',
            field=models.CharField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)], default=3, max_length=20, verbose_name='month'),
            preserve_default=True,
        ),
    ]
