from django.contrib import admin
from menstruation.models import Contact
from menstruation.models import SubscribeForm
from menstruation.models import Calculator
from menstruation.models import Conseil
from . import models
# Register your models here
#
admin.site.register(Contact)
admin.site.register(SubscribeForm)
admin.site.register(Calculator)
admin.site.register(Conseil)
admin.site.register(models.Article)
admin.site.register(models.Reporter)
