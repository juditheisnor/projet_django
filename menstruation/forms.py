__author__ = 'jb-flo'

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from menstruation.models import Contact
from menstruation.models import Calculator
from menstruation.models import Conseil


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact


class ConseilForm(forms.ModelForm):
    class Meta:
        model = Conseil


class CalculatorForm(forms.ModelForm):
    class Meta:
        model = Calculator


